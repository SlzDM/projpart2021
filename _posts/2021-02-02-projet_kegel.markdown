---
layout: post
title: Projet 5 & 8 - Bosquets St Georges & Potager
date: 2023-04-11
description: Petite description du projet 1
img: projet_kegel1.jpg # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [projet]
---

Le projet est porté par le Comité de Quartier « Kegeljan ». Celui-ci couvre une partie de Salzinnes et a pour but de promouvoir la qualité de vie dans le quartier à travers des activités festives et des démarches citoyennes. 

Le Comité de Quartier souhaite améliorer un espace semi-public sur le site de Kegeljan en y plantant notamment des arbres, en abaissant un mur (autorisant ainsi une meilleure visibilité et intégration avec le reste de l’espace public) et en adaptant les modules de jeux aux enfants et adolescents. 

Cet espace étant situé dans un quartier à forte densité de population, proche d'écoles fondamentales, de mouvements de jeunesse mais aussi de foyers d’hébergement d’enfants et d’adultes, l’objectif est de rendre l’endroit plus accueillant, ouvert à un public plus large favorisant ainsi le contrôle social mais aussi la convivialité et l’invitation à l’échange. 

En plantant des arbres, la biodiversité du lieu sera améliorée et l’endroit rendu plus agréable pour tous.

<b><a href="https://www.youtube.com/watch?v=z5pHNNaHOjE" target="_new">Visionner la vidéo de présentation</a></b>

## Contacts

Personnes de contact pour l’organisation et du suivi du projet :
- **Comité de quartier Kegeljan** - Damien Bioul
  - [Comité de quartier Kegeljan](mailto:comite@cqk.be)

## Images

![Photos Kegel]({{site.baseurl}}/assets/img/projet_kegel2.jpg)
